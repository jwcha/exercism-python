# exercism-python
My solutions for the Python challenges available at Exercism.io (API v2)

---

### Table of Contents
- <pre>001.</pre> Hello World
- <pre>002.</pre> Leap
